package pl.zybel;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.*;
import pl.zybel.adapter.CategoryListAdapter;
import pl.zybel.fragment.CategoryFragment;
import pl.zybel.task.PrepareApp;

public class MainActivity extends FragmentActivity {

    PrepareApp prepareApp;

    private boolean isProductsLoaded;
    private boolean isCategoriesLoaded;

    private ImageButton drawerToggle;
    private ListView categoryListView;
    private ImageView previousCategory;
    private DrawerLayout mDrawerLayout;
    private TextView currentCategoryName;
    private LinearLayout leftDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private CategoryListAdapter categoryListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        isProductsLoaded = false;
        isCategoriesLoaded = false;

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        leftDrawerLayout = (LinearLayout) mDrawerLayout.findViewById(R.id.left_drawer);

        previousCategory = (ImageView) findViewById(R.id.previousCategory);
        previousCategory.setTag(1);
        previousCategory.setVisibility(View.GONE);
        previousCategory.setOnClickListener(onPreviousCategoryClick);

        currentCategoryName = (TextView) leftDrawerLayout.findViewById(R.id.current_category);
        currentCategoryName.setText("Kategoria główna");

        drawerToggle = (ImageButton) findViewById(R.id.drawer_toggle);
        drawerToggle.setOnClickListener(onDrawerToggle);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer, R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerClosed(View view) {
            //    invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
            //    invalidateOptionsMenu();
            }
        };
        
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        categoryListView = new ListView(this);

        if (savedInstanceState == null) {
            prepareApp = new PrepareApp(MainActivity.this, this, categoryListView, leftDrawerLayout, categoryListAdapter);
            prepareApp.execute("1");

            setupPromoAnnouncement();
        }

        categoryListView.setCacheColorHint(Color.TRANSPARENT);
        categoryListView.requestFocus(0);

        categoryListView.setOnItemClickListener(onCategoryItemClickListener);
    }

    /**
     * Ładowanie promocyjnych ogłoszeń.
     */
    public void setupPromoAnnouncement() {
        Fragment fragment = new CategoryFragment();

        Bundle bundle = new Bundle();
        bundle.putInt(CategoryFragment.CATEGORY_ID, CategoryFragment.PROMO_ANNOUNCEMENT);
        fragment.setArguments(bundle);

        FragmentTransaction fragmentManager = getSupportFragmentManager().beginTransaction();
        fragmentManager.replace(R.id.content_frame, fragment, "CATEGORY_FRAGMENT");
        fragmentManager.addToBackStack(null);
        fragmentManager.commit();
    }

    AdapterView.OnClickListener onPreviousCategoryClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            prepareApp = new PrepareApp(MainActivity.this, MainActivity.this, categoryListView, leftDrawerLayout, categoryListAdapter);
            prepareApp.execute(""+previousCategory.getTag()+"");
        }
    };

    /**
     * Listener obsługujący klikniecie w liste z kategoriami.
     */
    AdapterView.OnItemClickListener onCategoryItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*Fragment fragment = new CategoryFragment();



            Bundle bundle = new Bundle();
            bundle.putInt(CategoryFragment.CATEGORY_ID, categoryListAdapter.getItem(position).getCategoryID());
            fragment.setArguments(bundle);

            FragmentTransaction fragmentManager = getSupportFragmentManager().beginTransaction();
            fragmentManager.replace(R.id.content_frame, fragment, "CATEGORY_FRAGMENT");
            fragmentManager.addToBackStack(null);
            fragmentManager.commit();*/

            //mDrawerLayout.closeDrawer(Gravity.START);

            categoryListAdapter = (CategoryListAdapter) categoryListView.getAdapter();

            if(categoryListAdapter.getItem(position).getHasChildren() > 0) {
                prepareApp = new PrepareApp(MainActivity.this, MainActivity.this, categoryListView, leftDrawerLayout, categoryListAdapter);
                prepareApp.execute(""+categoryListAdapter.getItem(position).getCategoryID()+"");

                if(categoryListAdapter.getItem(position).getCategoryParentID() != 1) {
                    previousCategory.setVisibility(View.VISIBLE);
                }
                previousCategory.setTag(categoryListAdapter.getItem(position).getCategoryParentID());
                currentCategoryName.setText(categoryListAdapter.getItem(position).getCategoryName());
            }


        }
    };

    AdapterView.OnClickListener onDrawerToggle = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(mDrawerLayout.isDrawerOpen(Gravity.START)) {
                mDrawerLayout.closeDrawer(Gravity.START);
            }else {
                mDrawerLayout.openDrawer(Gravity.START);
            }
        }
    };
}