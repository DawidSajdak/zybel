package pl.zybel.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import pl.zybel.R;
import pl.zybel.model.Category;

import java.util.List;

/**
 *
 * Created by Dawid on 24.07.13.
 */
public class CategoryListAdapter extends ArrayAdapter<Category> {

    Context activity;
    List<Category> categoryList;
    LayoutInflater layoutInflater;

    public CategoryListAdapter(Context activity, List<Category> categoryList) {
        super(activity, 0, categoryList);

        this.activity = activity;
        this.categoryList = categoryList;

        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    static class ViewHolder {
        public TextView text;
    }

    public int getCount() {
        return categoryList.size();
    }

    public Category getItem(int position) {
        return categoryList.get(position);
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View productListRow = convertView;
        final ViewHolder holder;

        if (productListRow == null) {

            productListRow = layoutInflater.inflate(R.layout.category_item, null);
            holder = new ViewHolder();
            holder.text = (TextView) productListRow.findViewById(R.id.category_name);
            productListRow.setTag(holder);
        } else {
            holder = (ViewHolder) productListRow.getTag();
        }

        holder.text.setText(categoryList.get(position).getCategoryName());
        return productListRow;
    }
}
