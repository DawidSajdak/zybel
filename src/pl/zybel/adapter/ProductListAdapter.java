package pl.zybel.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import pl.zybel.R;
import pl.zybel.model.Product;

import java.util.List;

/**
 *
 * Created by Dawid on 23.07.13.
 */
public class ProductListAdapter extends ArrayAdapter<Product> {

    Activity activity;
    List<Product> productList;
    LayoutInflater layoutInflater;

    public ProductListAdapter(Activity activity, List<Product> productList) {
        super(activity, 0, productList);

        this.activity = activity;
        this.productList = productList;

        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    static class ViewHolder {
        public TextView text;
        public ImageView coverImage;
    }

    public int getCount() {
        return productList.size();
    }

    public Product getItem(int position) {
        return null;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View productListRow = convertView;
        final ViewHolder holder;

        if (productListRow == null) {

            productListRow = layoutInflater.inflate(R.layout.product_category_item, null);

            holder = new ViewHolder();

            holder.text = (TextView) productListRow.findViewById(R.id.product_name);
            holder.coverImage = (ImageView) productListRow.findViewById(R.id.main_image);
            productListRow.setTag(holder);
        } else {
            holder = (ViewHolder) productListRow.getTag();
        }

        holder.text.setText(productList.get(position).getProductName());
        holder.coverImage.setImageResource(R.drawable.no_photo);
        return productListRow;
    }
}
