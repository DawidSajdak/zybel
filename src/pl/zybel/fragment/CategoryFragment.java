package pl.zybel.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import pl.zybel.R;
import pl.zybel.adapter.ProductListAdapter;
import pl.zybel.model.Product;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by Dawid Sajdak <dawid@trzy.it> on 15:09 - 04.07.13.
 */
public class CategoryFragment extends Fragment {

    public static final String CATEGORY_ID = "categoryID";

    public static final int PROMO_ANNOUNCEMENT = -1;

    Integer categoryID;
    ListView productListView;
    List<Product> productList;
    ProductListAdapter productListAdapter;

    public CategoryFragment() {}

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle savedInstanceState) {
        View view = layoutInflater.inflate(R.layout.loadin_products,container, false);

        categoryID = getArguments().getInt(CATEGORY_ID, PROMO_ANNOUNCEMENT);

        productList = new ArrayList<Product>();

        for(int i = 0; i < 10; i++) {
            Product product = new Product();
            product.setProductName("test -- " + i);

            productList.add(i, product);
        }

        /*productListView = (ListView) view.findViewById(R.id.category_list);
        productListAdapter = new ProductListAdapter(getActivity(), productList);
        productListView.setAdapter(productListAdapter);

        productListView.setOnItemClickListener(onProductItemClickListener);*/

        return view;
    }


    AdapterView.OnItemClickListener onProductItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Fragment fragment = new ProductFragment();

            Bundle bundle = new Bundle();
            bundle.putParcelable("productObject", productList.get(position));
            fragment.setArguments(bundle);

            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();


        }
    };
}
