package pl.zybel.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import pl.zybel.model.Product;

/**
 *
 * Created by Dawid Sajdak <dawid@trzy.it> on 15:09 - 04.07.13.
 */
public class ProductFragment extends Fragment {

    Product product;

    public static final String PRODUCT_ID = "categoryID";

    public ProductFragment() {}

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle savedInstanceState) {

        if(savedInstanceState != null) {

        }

        product = getArguments().getParcelable("productObject");
        System.out.println(product.getProductName() + " __ " + product.getProductID());

        return null;
    }

}
