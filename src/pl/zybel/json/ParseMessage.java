package pl.zybel.json;

import android.os.AsyncTask;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import pl.zybel.model.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 *
 * Created by Dawid Sajdak <dawid@trzy.it> on 10.07.13.
 */
public class ParseMessage {

    private static JSONArray jsonArray;

    public static List<Category> parseCategory() throws JSONException, ExecutionException, InterruptedException {

        Exec exec = new Exec();
        exec.execute();



        while (exec.get() != null) {
            try {
                jsonArray = new JSONArray(exec.get());
                List<Category> categoryList = new ArrayList<Category>();

                for(int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    Category category = new Category();
                    category.setCategoryID(jsonObject.getInt("categoryID"));
                    category.setCategoryName(jsonObject.getString("categoryName"));
                    category.setHasChildren(jsonObject.getInt("hasChildren"));

                    categoryList.add(i, category);
                }

                return categoryList;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            break;
        }

        return null;
    }
}
