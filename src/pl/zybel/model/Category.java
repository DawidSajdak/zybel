package pl.zybel.model;

/**
 *
 * Created by Dawid Sajdak <dawid@trzy.it> on 15:10 - 04.07.13.
 */
public class Category {

    private Integer categoryID;

    private String categoryName;

    private Integer hasChildren;

    private Integer categoryParentID;

    public Integer getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Integer categoryID) {
        this.categoryID = categoryID;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Integer getHasChildren() {
        return hasChildren;
    }

    public void setHasChildren(Integer hasChildren) {
        this.hasChildren = hasChildren;
    }

    public Integer getCategoryParentID() {
        return categoryParentID;
    }

    public void setCategoryParentID(Integer categoryParentID) {
        this.categoryParentID = categoryParentID;
    }
}
