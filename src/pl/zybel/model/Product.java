package pl.zybel.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 *
 * Created by Dawid Sajdak <dawid@trzy.it> on 15:10 - 04.07.13.
 */
public class Product implements Parcelable {
    /**
     * @var int
     */
    private int productID;

    /**
     * @var String
     */
    private String productName;

    /**
     * @var String
     */
    private String createdDate;

    /**
     * @var String
     */
    private String coverPhoto;

    /**
     * @var String
     */
    private String productPrice;

    /**
     * @var Integer
     */
    private Integer productViews;

    /**
     * @var String
     */
    private String productDistrict;

    /**
     * @var String
     */
    private String productCity;

    /**
     * @var String
     */
    private String productStreet;

    /**
     * @var String
     */
    private String productPhone;

    /**
     * @var String
     */
    private String productWebsite;

    /**
     * @var String
     */
    private String productQuantity;

    /**
     * @var String
     */

    private String productZipCode;

    /**
     * @var String
     */
    private String productDescription;


    public Product() {}

    public Product(Parcel in) {
        readFromParcel(in);
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductZipCode() {
        return productZipCode;
    }

    public void setProductZipCode(String productZipCode) {
        this.productZipCode = productZipCode;
    }

    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getProductWebsite() {
        return productWebsite;
    }

    public void setProductWebsite(String productWebsite) {
        this.productWebsite = productWebsite;
    }

    public String getProductPhone() {
        return productPhone;
    }

    public void setProductPhone(String productPhone) {
        this.productPhone = productPhone;
    }

    public String getProductStreet() {
        return productStreet;
    }

    public void setProductStreet(String productStreet) {
        this.productStreet = productStreet;
    }

    public String getProductCity() {
        return productCity;
    }

    public void setProductCity(String productCity) {
        this.productCity = productCity;
    }

    public String getProductDistrict() {
        return productDistrict;
    }

    public void setProductDistrict(String productDistrict) {
        this.productDistrict = productDistrict;
    }

    public Integer getProductViews() {
        return productViews;
    }

    public void setProductViews(Integer productViews) {
        this.productViews = productViews;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeInt(productID);
        parcel.writeInt(productViews);
        parcel.writeString(productName);
        parcel.writeString(productDescription);
        parcel.writeString(productZipCode);
        parcel.writeString(productQuantity);
        parcel.writeString(productWebsite);
        parcel.writeString(productPhone);
        parcel.writeString(productStreet);
        parcel.writeString(productCity);
        parcel.writeString(productDistrict);
        parcel.writeString(productPrice);
        parcel.writeString(coverPhoto);
        parcel.writeString(createdDate);

    }

    private void readFromParcel(Parcel in) {
        productID = in.readInt();
        productViews = in.readInt();
        productName = in.readString();
        createdDate = in.readString();
        productDescription = in.readString();
        productZipCode = in.readString();
        productQuantity = in.readString();
        productWebsite = in.readString();
        productPhone = in.readString();
        productStreet = in.readString();
        productCity = in.readString();
        productDistrict = in.readString();
        productPrice = in.readString();
        coverPhoto = in.readString();
    }
}
