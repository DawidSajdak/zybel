package pl.zybel.task;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import pl.zybel.R;
import pl.zybel.adapter.CategoryListAdapter;
import pl.zybel.fragment.CategoryFragment;
import pl.zybel.json.ParseMessage;
import pl.zybel.model.Category;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 *
 * Created by Dawid on 07.08.13.
 */
public class PrepareApp extends AsyncTask<String, String, List<Category>> {

    private Context context;
    private Activity activity;
    private ListView categoryListView;
    private LinearLayout leftDrawerLayout;
    private CategoryListAdapter categoryListAdapter;

    public PrepareApp(Activity activity, Context context, ListView categoryListView, LinearLayout leftDrawerLayout, CategoryListAdapter categoryListAdapter) {
        this.context = context;
        this.activity = activity;
        this.categoryListView = categoryListView;
        this.leftDrawerLayout = leftDrawerLayout;
        this.categoryListAdapter = categoryListAdapter;
    }


    @Override
    protected List<Category> doInBackground(String... params) {
        List<Category> categoryList = null;

        try {

            JSONArray jsonArray = new JSONArray(getJsonArray(params[0]));

            categoryList = new ArrayList<Category>();

            for(int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                Category category = new Category();
                category.setCategoryID(jsonObject.getInt("categoryID"));
                category.setCategoryName(jsonObject.getString("categoryName"));
                category.setHasChildren(jsonObject.getInt("hasChildren"));
                category.setCategoryParentID(jsonObject.getInt("categoryParentID"));

                categoryList.add(i, category);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return categoryList;
    }

    @Override
    public void onPostExecute(List<Category> categoryList) {

        categoryListAdapter = new CategoryListAdapter(context, categoryList);
        categoryListView.setAdapter(categoryListAdapter);

        if(leftDrawerLayout.getChildCount() == 1) {
            leftDrawerLayout.addView(categoryListView);
        }
    }

    private String getJsonArray(String categoryID) {

        String result = null;
        InputStream inputStream = null;
        ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();

        try {
            HttpClient httpClient = new DefaultHttpClient();

            HttpPost httpPost = new HttpPost("http://www.zybel.pl/api.php?categoryID=" + categoryID);
            httpPost.setEntity(new UrlEncodedFormEntity(param));
            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();

            inputStream = httpEntity.getContent();
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            e3.printStackTrace();
        } catch (IOException e4) {
            e4.printStackTrace();
        }

        try {
            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 8);
            StringBuilder sBuilder = new StringBuilder();

            String line;
            while ((line = bReader.readLine()) != null) {
                sBuilder.append(line + "\n");
            }

            inputStream.close();
            result = sBuilder.toString();


        } catch (Exception e) {
            Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
        }

        return result;
    }
}
